const express = require("express");
const http = require("http");
const dotenv = require("dotenv");
const helmet = require("helmet");
const socketio = require("socket.io");

dotenv.config({ path: "./config/config.env" });

const PORT = process.env.PORT || 9000;

const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(helmet());
app.use(express.json());

io.on("connection", (socket) => {
  // Welcome current user
  socket.emit("message", "Welcome to Chat!");

  // Broadcast when user connects
  socket.broadcast.emit("message", "A user has joined the chat");

  // Runs when client disconnects
  socket.on("disconnect", () => {
    io.emit("message", "A user has left the chat");
  });
});

server.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
