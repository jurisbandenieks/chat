import { Injectable } from "@angular/core";
import { Socket } from "ngx-socket-io";

@Injectable({
  providedIn: "root"
})
export class ChatService {
  constructor(private socket: Socket) {}

  getMessage() {
    this.socket.on("message", (message) => {
      console.log(message);
    });
  }
}
