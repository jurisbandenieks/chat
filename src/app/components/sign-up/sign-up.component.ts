import { Component, ViewEncapsulation } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class SignUpComponent {
  constructor() {}

  ngOnInit(): void {}
}
